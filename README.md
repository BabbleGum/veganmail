# VeganMail

Hallo zusammen,  
 
hier wie gewünscht ein paar Informationen zum vegan sein.  
Falls du mehr Detailinformationen oder so möchtest, stehe ich dir jederzeit gern zur Verfügung.  
 
# Einkaufsguide
Der Einkaufsguide enthält gruppiert nach Produkten alle möglichen vegane Produkte, die im normalen Handel wie Edeka, Lidl, Rewe, DM usw. erhältlich sind  
Gibt es auch als APP 😊  
https://www.petazwei.de/einkaufsguide

# Vegane Rezepte
vegane Rezepte findet man fast überall.  
z.B. bei https://www.chefkoch.de/rs/s0t32,57/Vegan/Vegetarisch-Vegan-Rezepte.html  
oder (falls du was Besonderes machen willst) : https://www.bosh.tv/ / https://de-de.facebook.com/bosh.tv/

# Dokus / Videos:
„Must have (am besten in dieser Reihenfolge): “  

**Gabel statt Skalpell**  
Doku über den Zusammenhang von Ernährung und Krankheit  
Vegan nicht direkt Thema, aber gut und interessant  

**What the Health**  
Doku über Ernährung   
Konkret über tierische Produkte und die Auswirkung auf unsere Gesundheit.  
Fokus auf „große“ Krankheiten wie Krebs, Diabetes und Herzleiden.  

**Cowspiracy**  
Doku über Viehwirtschaft in Bezug auf Umwelt  

**Earthlings**  
Doku über Nutztierhaltung und Fleischkonsum  
Recht direkte Bilder…aber wirkungsvoll…

# Diverses:
**Vegan ist ungesund**  
Youtube Channel über alle möglichen Themen aus veganer Sicht.  
Wenn man den Humor mag sehr unterhaltsam. Quellenangaben jeweils unter den Videos.  
Jeweils nur kurze Videos von unter 10 min.  
Die Live und Koch Sachen kann man sich sparen 😊  
https://www.youtube.com/channel/UCURHLn3nl9AFVeD1G0lnlaw/videos  

**Watch1000eyes**  
5 Minuten Video. Unkommentierter Zusammenschnitt aus der Massentierhaltung.  
Recht direkte Bilder…aber wirkungsvoll…  
http://watch1000eyes.com/  

**Diskussion** zwischen einem Veganer und zwei Studenten.  
Sehr interessante Ansätze in dem Gespräch und vernünftig geführt (in Englisch)  
https://www.youtube.com/watch?v=Sq8AV9HK3z4

# Kosmetik 
(gut recherchierte App zu Tierversuchen)   
Tierversuchfreie Kosmetik wie z.B. Balea, Lush Alverde (die meisten Produkte davon auch vegan)  
https://play.google.com/store/apps/details?id=net.sekl.kosmetikappv2&hl=de  